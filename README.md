I intend to create an entire, original song piece entirely through code.

Here is the SoundCloud to listen to the code music: https://soundcloud.com/stuart-ashby

I am using a a variant of the Ruby programming language and Sonic Pi. Your feedback is welcome friends.

Experimental: [sonic_pi_1.rb](https://bitbucket.org/alliedtechs/creating-music-via-the-power-of-code/src/a81281f61dee47905d569bfc7b81a233d436357b/sonic_pi_1.rb), [sonic_pi_6.rb](https://bitbucket.org/alliedtechs/creating-music-via-the-power-of-code/src/a81281f61dee47905d569bfc7b81a233d436357b/sonic_pi_6.rb) and [sonic_pi_7.rb](https://bitbucket.org/alliedtechs/creating-music-via-the-power-of-code/src/a81281f61dee47905d569bfc7b81a233d436357b/sonic_pi_7.rb)

Development: [**drumbeat.rb**](https://bitbucket.org/alliedtechs/creating-music-via-the-power-of-code/src/21a654e5be0a11aa56d20d7a860bde5d74494c54/drumbeat.rb)