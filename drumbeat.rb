define :drumbeat do |bpm,n|
  use_bpm bpm
  in_thread do
    4.times do
      sleep 2
      sample :drum_cymbal_open, amp: 0.10, attack: 5
      sleep 2
    end
  end
  in_thread do
    4.times do
      sleep 1
      if n==2 || n==4
        with_fx :reverb do
          sample :bass_woodsy_c, amp: 1.5
        end
      end
      sleep 3
    end
  end
  in_thread do
    4.times do
      sleep 1.5
      with_fx :reverb do
        sample :drum_cymbal_open, amp: 0.25
      end
      sleep 2.5
    end
  end
  in_thread do
    8.times do
      sleep 1.5
      sample :drum_bass_hard, amp: 2.5
      sleep 0.5
    end
  end
  in_thread do
    8.times do
      sleep 1
      sample :bd_boom, amp: 2
      sleep 1
    end
  end
  in_thread do
    16.times do
      sample :drum_cymbal_closed, amp: 2
      sleep 1
    end
  end
  in_thread do
    16.times do
      sample :drum_snare_hard, amp: 2
      sleep 1
    end
  end
  in_thread do
    32.times do
      sample :bd_808, amp: 3
      sleep 0.5
    end
  end
  in_thread do
    32.times do
      sleep 0.25
      sample :bd_pure, amp: 4
      sleep 0.25
    end
  end
  in_thread do
    64.times do
      sample :drum_heavy_kick, amp: 3
      sleep 0.25
    end
  end
end
i=1
b=60
4.times do
  3.times do
    sample :drum_heavy_kick, amp: 1
    sample :drum_cymbal_closed, amp: 1
    sample :drum_snare_hard, amp: 1
    sleep 1
  end
  drumbeat(b,i)
  sleep 16
  #can change params to change up beats with each 16 bar loop
  i+=1
  b+=1 #make beat a little faster (or you can slow it down if you'd like) on subsequent iterations
end
3.times do
  sample :drum_heavy_kick, amp: 1
  sample :drum_cymbal_closed, amp: 1
  sample :drum_snare_hard, amp: 1
  sleep 1
end
drumbeat(58,0)
