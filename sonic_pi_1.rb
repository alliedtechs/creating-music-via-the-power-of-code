#drum loop
live_loop :drums do
  beatlength = 0.5
  sample :drum_heavy_kick
  sleep beatlength
  sample :drum_snare_hard
  sleep beatlength/2
  sample :drum_heavy_kick
  sleep beatlength
  sample :drum_snare_hard
  sleep beatlength
end

#guitar loop
live_loop :guit do
  with_fx :echo, mix: 0.3, phase: 0.25 do
    sample :guit_em9, rate: 0.5
  end
  sleep 8
end

#boom loop
live_loop :boom do
  with_fx :reverb, room: 1 do
    sample :bd_boom, amp: 10, rate: 1
  end
  sleep 8
end