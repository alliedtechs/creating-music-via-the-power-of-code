#DEFINE GLOBALS
br1=br2=br3=br4=br5=br6=br7=br8=br9=br10=br11=br12=br13=br14=br15=0
ba1amp=ba1loop=ba1boostup=0
ba2amp=ba2loop=ba2boostup=0
ba3amp=ba3loop=ba3boostup=0
bass1=bass2=bass3=""

#INSTRUMENT SETUP & CONTROL FUNCTIONS
define :setup_bass do |s|
  #BEAT RATE
  br1=1*s
  br2=2*s
  br3=3*s
  br4=4*s
  br5=6*s
  br6=7*s
  br7=8*s
  br8=12*s
  br9=14*s
  br10=16*s
  br11=24*s
  br12=26*s
  br13=28*s
  br14=32*s
  br15=48*s
  #BASS1
  bass1="bd_808"
  ba1amp=4
  ba1loop=1
  ba1boostup=0
  #BASS2
  bass2="bass_woodsy_c"
  ba2amp=1
  ba2loop=4
  ba2boostup=1
  #BASS3
  bass3="bd_fat"
  ba3amp=10
  ba3loop=2
  ba3boostup=0
end

#INSTRUMENT FUNCTIONS
define :fbass do |x,amp,samp,t,boostamp|
  i=boostamp
  t.times do
    sample samp,amp:1+i,start:0.01,finish: 1
    sleep x
    i=i+boostamp
  end
end
define :fbassroll do |x,amp,samp,t,boostamp|
  fbass(x,amp,samp,t,boostamp)
  fbass(x/2,amp/2,samp,t,boostamp)
  fbass(x-1,amp-1,samp,t,boostamp)
  fbass(x,amp,samp,t,boostamp)
end

#PLAY FUNCTIONS
define :playloop1 do |t1loop,t1sleep|
  #bass loop thread
  fbass(br3,ba1amp,bass1,ba1loop,ba1Sboostup)
  sleep t1sleep
  in_thread do
    i=1
    t1loop.times do
      fbassroll(br6,ba1amp,bass1,ba1loop,ba1boostup)
      fbass(br2,ba1amp,bass1,ba1loop,ba1boostup)
      fbass(br2,ba3amp,bass3,ba3loop,ba3boostup)
      fbass(br3,ba2amp,bass2,ba2loop,ba2boostup)
      if i<t1loop
        fbassroll(br5/1.5,ba1amp,bass1,ba1loop,ba1boostup)
      end
      i=i+1
    end
    sleep t1sleep
  end
  #end bass loop
end

#MAIN LOOP
with_fx :reverb do
  setup_bass(0.125)
  thread1loop=3
  thread1sleep=0.02
  playloop1(thread1loop,thread1sleep)
end