#DEFINE GLOBALS
br1=br2=br3=br4=br5=br6=br7=br8=br9=br10=br11=br12=br13=br14=br15=0
ba1amp=ba1loop=ba1boostup=0
ba2amp=ba2loop=ba2boostup=0
ba3amp=ba3loop=ba3boostup=0
bass1=bass2=bass3=""
n1=d1=s1=a1=r1=""
n2=d2=s2=a2=r2=""
n3=d3=s3=a3=r3=""
n4=d4=s4=a4=r4=""

#INSTRUMENT SETUP & CONTROL FUNCTIONS
define :setup_inst do |s|
  #BEAT RATE
  br1=1*s
  br2=2*s
  br3=3*s
  br4=4*s
  br5=6*s
  br6=7*s
  br7=8*s
  br8=12*s
  br9=14*s
  br10=16*s
  br11=24*s
  br12=26*s
  br13=28*s
  br14=32*s
  br15=48*s
  #BASS1
  bass1="bd_808"
  ba1amp=4
  ba1loop=1
  ba1boostup=0
  #BASS2
  bass2="bass_woodsy_c"
  ba2amp=1
  ba2loop=4
  ba2boostup=1
  #BASS3
  bass3="bd_fat"
  ba3amp=10
  ba3loop=2
  ba3boostup=0
  #tunes
  n1 = [:g4,:ab4]+trn(:ab4,14)
  d1 = [br4,br5]+trd(br6+br7,14)
  s1 = 1
  a1 = 1
  r1 = 1
  n2 = [:e4,:f4,:f4,:e4,:r,:eb4]
  d2 = [br1,br2,br3,br4,br5,br6]
  s2 = 1
  a2 = 1
  r2 = 1
  n3 = [:c4,:c4,:c4,:c4,:r,:c4,:c4,:c4]
  d3 = [br8,br7,br6,br5,br4,br3,br2,br1]
  s3 = 1
  a3 = 1
  r3 = 1
  n4 = [:c3,:f3,:f3,:c3,:r,:c3,:ab3,:ab3,:g3,:r]
  d4 = [br1,br2,br3,br4,br5,br6,br7,br8,br9,br10]
  s4 = 1
  a4 = 1
  r4 = 1
end
define :trn do |n,num,offset=2| #produces trill sequence
  n = note_info(n).midi_note
  return [n+offset,n]*(num / 2)
end
define :trd do |d,num| #produces trill note durations
  return [d/num]*num
end

#INSTRUMENT FUNCTIONS
define :fbass do |x,amp,samp,t,boostamp|
  i=boostamp
  t.times do
    sample samp,amp:1+i,start:0.01,finish: 1
    sleep x
    i=i+boostamp
  end
end
define :fbassroll do |x,amp,samp,t,boostamp|
  fbass(x,amp,samp,t,boostamp)
  fbass(x/2,amp/2,samp,t,boostamp)
  fbass(x-1,amp-1,samp,t,boostamp)
  fbass(x,amp,samp,t,boostamp)
end
define :tune do|pitch,duration,shift,amp,ratio|
  pitch.zip(duration).each do |p,d|
    if p == :r
      sleep d
    else
      with_transpose shift do
        play p,sustain: ratio*d,release: (1-ratio)*d,attack: 0,amp: amp
        sleep d
      end
    end
  end
end

#PLAY FUNCTIONS
define :playloop1 do |t1loop,t1sleep|
  #bass loop thread
  fbass(br3,ba1amp,bass1,ba1loop,ba1boostup)
  sleep t1sleep
  in_thread do
    i=1
    t1loop.times do
      fbassroll(br6,ba1amp,bass1,ba1loop,ba1boostup)
      fbass(br2,ba1amp,bass1,ba1loop,ba1boostup)
      fbass(br2,ba3amp,bass3,ba3loop,ba3boostup)
      fbass(br3,ba2amp,bass2,ba2loop,ba2boostup)
      if i<t1loop
        fbassroll(br5/1.5,ba1amp,bass1,ba1loop,ba1boostup)
      end
      i=i+1
    end
    sleep t1sleep
  end
  #end bass loop
  #tune loops
  in_thread do
    i=1
    t1loop.times do
      tune(n1,d1,s1,a1,r1)
      i=i+1
    end
  end
  in_thread do
    i=1
    t1loop.times do
      tune(n2,d2,s2,a2,r2)
      i=i+1
    end
  end
  in_thread do
    i=1
    t1loop.times do
      tune(n3,d3,s3,a3,r3)
      i=i+1
    end
  end
  in_thread do
    i=1
    t1loop.times do
      tune(n4,d4,s4,a4,r4)
      i=i+1
    end
  end
  #end tune loops
end

#MAIN LOOP
with_fx :reverb do
  setup_inst(0.125)
  thread1loop=3
  thread1sleep=0.02
  playloop1(thread1loop,thread1sleep)
end